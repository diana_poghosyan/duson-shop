$(document).ready(function(){

    var docWidth = $( window ).width();
    var docHeight = $( window ).height();

    $('.basket-dropdown').on('click', '.basket-dropdown-toggle',function() {
        if (docWidth < 767) {
            $('body').toggleClass('overflow-y-hidden');
        }
    });
    $('.btn-close').click(function() {
        $('#sidebar').removeClass('visible');
        if (docWidth < 767) {
            $('body').toggleClass('overflow-y-hidden');
        }
    });
    $('.btn-filters').click(function() {
        $('.collapsed-xs').slideToggle();

    });
    if( docWidth > 768 && docWidth < 991){
        $('header').find($('input.input-search')).attr("placeholder","");
    }
        $('#sidebar-btn').click(function() {
            $('#sidebar').toggleClass('visible');
            if( docWidth < 768) {
                $('#sidebar').css("overflow-y", "auto");
                $('body').css("overflow-y", "hidden");
            }
            else {
                $('#sidebar').css("overflow-y", "hidden");
                $('body').css("overflow-y", "visible");
            }
        });

    $(".welcome-section").on("click",".btn-scroll-down",function() {
        $('html, body').animate({
            scrollTop: $(".products").offset().top
        }, 2000);
    });
    $(".dropdown").on("click",".dropdown-menu *",function(event) {
        event.stopPropagation();
    });

    $(".basket-dropdown, .basket").on("click",".btn-remove",function(event) {
        event.stopPropagation();
        var removeItemId = $( this ).data('remove-item');
        $('.basket-remove-confirm[data-confirm-item = "'+removeItemId+'"]').show();
    });

    $(".basket-dropdown, .basket").on("click",".basket-remove-confirm .btn-confirm",function(event) {
        event.stopPropagation();
        var confirmItemId = $( this ).data('confirm');
        $('.basket-item-row[data-basket-item = "'+confirmItemId+'"]').remove();
    });

    $(".basket-dropdown, .basket").on("click",".basket-remove-confirm .btn-restore",function(event) {
        event.stopPropagation();
        var restoreItemId = $( this ).data('restore');
        $('.basket-item-row[data-basket-item = "'+restoreItemId+'"]').remove();
    });
    $(".basket-dropdown,.basket").on("click",".icon-mobile_close-01",function(event) {
        $('.dropdown.basket-dropdown.open').removeClass('open');
    });
    $(window).resize(function(){
        var docWidth = $( window ).width();
        if( docWidth > 768 && docWidth < 991){
            $('header').find($('input.input-search')).attr("placeholder","");
        }
        var maxHeight = -1;

        $('.products .product-item, .products-list-slider .product-item').each(function() {
            maxHeight = maxHeight > $(this).height() ? maxHeight :     $(this).height();
        });

        $('.products .product-item, .products-list-slider .product-item').each(function() {
            $(this).height(maxHeight);
        });
    });
    var maxHeight = -1;

    $('.products .product-item, .products-list-slider .product-item').each(function() {
        maxHeight = maxHeight > $(this).height() ? maxHeight :     $(this).height();
    });

    $('.products .product-item, .products-list-slider .product-item').each(function() {
        $(this).height(maxHeight);
    });


    if( docWidth > 768){
        var maxHeight = -1;
        $('body').find('.offers-desc').each(function() {
            maxHeight = maxHeight > $(this).height() ? maxHeight :     $(this).height();
        });

        $('.offers-desc').each(function() {
            $(this).height(maxHeight);
        });
    }
    if( docWidth > 991) {
        var reviewboxheight = $('.review-boxes').height() - 10;
        $('.article-box').css('height',reviewboxheight );

    }
    $('.collapse').on('show.bs.collapse', function() {
        var toggle = $('[data-target="#' + this.id + '"]');
        if (toggle) {
            var parent = toggle.attr('data-parent');
            if (parent) {
                $(parent).find('.collapse.in').collapse('hide');
            }
        }
    });
    $('.collections-tab').on('click', 'li a', function() {
        $('.default-col').hide();
    });
    $('.list-group-white').on('click', 'li', function() {
        $('.list-group-white li.active').removeClass('active');
        $(this).addClass('active');
    });
    $('.product-sort').on('click', 'button', function() {
        $(this).siblings().removeClass('active')
        $(this).addClass('active');
    });
    $('.shipping-address').on('click', '.change-address', function() {
        $('.current-address').hide();
        $('.new-addres-settings').removeClass('hidden');
    });

    $('.shipping-address').on('submit', '.btn-save', function() {
        $('.new-addres-settings').addClass('hidden');
        $('.current-address').show();

    });
    $('.category-tags').on('click', '.icon-delete_filter-01', function() {
        $(this).parents('.category-tags-item').remove();
    });

    $('.talk-to-expert').on('click', '.panel-heading .btn-talk-to-expert', function() {
        $('.talk-to-expert .panel-chat').slideToggle();
    });

    $("#slider").slider({
        range: true,
        min: 0,
        max: 100000,
        step: 500,
        values: [18000, 80000],
        slide: function(event, ui) {
            var delay = function() {
                //var handleIndex = $(ui.handle).data('index.uiSliderHandle');
                var handleIndex = $(ui.handle).index();
                var label = handleIndex == 1 ? '#min' : '#max';
                $(label).html(ui.value).position({
                    my: 'center top',
                    at: 'center bottom',
                    of: ui.handle,
                    offset: "0, 500"
                });
            };

            // wait for the ui.handle to set its position
            setTimeout(delay, 5);
        }
    });

    $('#min').html($('#slider').slider('values', 0)).position({
        my: 'center top',
        at: 'center bottom',
        of: $('#slider span:eq(0)'),
        offset: "0, 500"
    });

    $('#max').html($('#slider').slider('values', 1)).position({
        my: 'center top',
        at: 'center bottom',
        of: $('#slider span:eq(1)'),
        offset: "0, 500"
    });

});
var docWidth = $( document ).width();
if( docWidth >= 1200){
    var charLimit = 561;
}
if( docWidth >= 768 && docWidth < 1200){
    var charLimit = 400;
}
if( docWidth < 768){
    var charLimit = 200;
}

function truncate(el) {
    var clone = el.children().first(),
        originalContent = el.html(),
        text = clone.text();

    if(clone[0].innerHTML.trim().length>charLimit){
        el.attr("data-originalContent", originalContent);
        el.addClass('hasHidden');
        clone.text(text.substring(0, charLimit) + "...")
        el.empty().append(clone);
        el.append($("<div class='read-more'><a href='#' class='more'>Read More</a>"));
    }
}

$("body").on("click",'a.more',function (e) {
    e.preventDefault();
    var truncateElement = $(this).parent().parent();
    if(truncateElement.hasClass('hasHidden')){
        $(truncateElement).html(truncateElement.attr("data-originalContent"));
        $(truncateElement).append($("<div class='read-more'><a href='#' class='more'>Read Less</a>"));
        truncateElement.removeClass('hasHidden');
    }
    else{
        $('.read-more',truncateElement).remove();
        truncate(truncateElement);
    }
});

$(".info-text").each(function () {
    truncate($(this));
});


