//SET THESE VARS
var $transitionLengthModal = 400;
var $timeBetweenTransitionsModal = 4000;

//STORAGE
var imageCountModal = 0;
var currentImageIndexModal = 0;
var currentScrollIndexModal = 1;
var $imageBankModal = [];
var $thumbBankModal = [];
var $mainContainerModal = $("#gallery-main-modal");
var $thumbContainerModal = $("#thumbcon-modal");
var $progressModalBarModal = $("#progressModalbar-modal");
var currentElementModal;

//CONTROLS
var $goModal = false;

$(document).ready(function(){
	// $('#playtoggle').click();

	$("#gallery-hidden-modal img").each(function() {
		$imageBankModal.push($(this).attr("id", imageCountModal));
        imageCountModal++;
	});

	generateThumbsModal();

	setTimeout(function () {
		imageScrollModal(0);
	}, $timeBetweenTransitionsModal);

	$('#left-arrow-modal').click(function () {
		thumbScrollModal("left");
		toggleScrollModal(true);
    });

	$('#right-arrow-modal').click(function () {
		thumbScrollModal("right");
		toggleScrollModal(true);
    });

	$('#thumbcon-modal img').on('click',function () {
		imageFocusModal(this);
	});

	$('#playtoggle-modal').click(function () {
		toggleScrollModal(false);
	});
});

function progressModal(imageIndex){
	var parts = 960/imageCountModal-1;
	var pxProgressModal = parts*(imageIndex+1);

	$progressModalBarModal.css({ width: pxProgressModal , transition: "all 0.7s ease"});
}

function imageFocusModal(focus){
	for(var i = 0; i < imageCountModal; i++){
		if($imageBankModal[i].attr('src') == $(focus).attr('src')){
			$mainContainerModal.fadeOut($transitionLengthModal);
			$thumbBankModal[currentImageIndexModal].removeClass("selected");
			setTimeout(function () {
				$mainContainerModal.html($imageBankModal[i]);
				$thumbBankModal[i].addClass("selected");
				$mainContainerModal.fadeIn($transitionLengthModal);
			}, $transitionLengthModal);
			currentScrollIndexModal = i+1;
            currentImageIndexModal = i;
			progressModal(currentImageIndexModal);
			toggleScrollModal(true);

			return false;
		}
	}
}

function toggleScrollModal(bool){
	if($goModal){
		$goModal = false;
		$('#playtoggle-modal').children().removeClass('icon-pause').addClass('icon-play');
	}else{
		$goModal = true;
		$('#playtoggle-modal').children().removeClass('icon-play').addClass('icon-pause');
	}

	if(bool){
		$goModal = false;
		$('#playtoggle-modal').children().removeClass('icon-pause').addClass('icon-play');
	}
}

function autoScrollModal(){
	if(currentScrollIndexModal >= 0 || currentScrollIndexModal < imageCountModal){
		if(currentScrollIndexModal+1 > imageCountModal){
			$thumbBankModal[0].css({ marginLeft: "0" , transition: "all 1.0s ease"});
			currentScrollIndexModal = 1;
		}else if(currentScrollIndexModal+1 >= 3){
			if(currentScrollIndexModal+2 >= imageCountModal){

			}else
				$thumbBankModal[0].css({ marginLeft: "-=200" , transition: "all 1.0s ease"});

			currentScrollIndexModal++;
		}else{
			currentScrollIndexModal++;
		}
	}
}

function thumbScrollModal(direction){
	if(currentScrollIndexModal >= 0 || currentScrollIndexModal < imageCountModal){
		var marginTemp = currentScrollIndexModal;
		if(direction == "left"){
			if(currentScrollIndexModal-7 <= 0){
				var k = ((imageCountModal-7)*145)-2;
				$thumbBankModal[0].css({ marginLeft: -k , transition: "all 1.0s ease"});
				currentScrollIndexModal = imageCountModal-1;
			}else{
				$thumbBankModal[0].css({ marginLeft: "+=124" , transition: "all 1.0s ease"});
				currentScrollIndexModal--;
			}
		}else if(direction == "right"){
			if(currentScrollIndexModal+7 >= imageCountModal){
				$thumbBankModal[0].css({ marginLeft: "2px" , transition: "all 1.0s ease"});
				currentScrollIndexModal = 1;
			}else{
				$thumbBankModal[0].css({ marginLeft: "-=124" , transition: "all 1.0s ease"});
				currentScrollIndexModal++;
			}
		}
	}
}

function generateThumbsModal(){
	progressModal(currentImageIndexModal);
	for(var i = 0; i < imageCountModal; i++){

		var $tempObj = $('<img id="'+i+'t" class="thumb" src="'+$imageBankModal[i].attr('src')+'" />');

		if(i == 0)
			$tempObj.addClass("selected");

		$thumbContainerModal.append($tempObj);
		$thumbBankModal.push($tempObj);

	}
}

function imageScrollModal(c){
	if($goModal){

		$thumbBankModal[c].removeClass("selected");

		c++

		if(c == $imageBankModal.length)
			c = 0;

		$mainContainerModal.fadeOut($transitionLengthModal);
		setTimeout(function () {
			$mainContainerModal.html($imageBankModal[c]);
			$thumbBankModal[c].addClass("selected");
			autoScrollModal("left");
			$mainContainerModal.fadeIn($transitionLengthModal);
		}, $transitionLengthModal);

	}

	progressModal(c);

	setTimeout(function () {
		imageScrollModal(currentImageIndexModal);
	}, $timeBetweenTransitionsModal);

    currentImageIndexModal = c;
}

